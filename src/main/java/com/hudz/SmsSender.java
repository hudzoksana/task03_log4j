package com.hudz;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsSender {
    // Find your Account Sid and Auth Token at twilio.com/console
    public static final String ACCOUNT_SID =
            "AC246a22ff9b62ea316991632d15412017";
    public static final String AUTH_TOKEN =
            "d37f398ad2be54027b409ef9bcc0c476";

    public static void send(String string) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message
                .creator(new PhoneNumber("+380507025379"), // to
                        new PhoneNumber("+12025707522"), // from
                        string)
                .create();

        System.out.println(message.getSid());
    }
}