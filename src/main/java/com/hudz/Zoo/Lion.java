package com.hudz.Zoo;

import com.hudz.Forest.Oak;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Lion extends Animal {
    private static final Logger logger = LogManager.getLogger(Lion.class.getName());
    public String voice() {
        logger.info("This is an info message");
        logger.trace("This is a trace message");

        return "I`m Lion";
    }
}
