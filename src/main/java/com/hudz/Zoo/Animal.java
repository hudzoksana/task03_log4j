package com.hudz.Zoo;

public abstract class Animal {
    private String name;
    public abstract String voice();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
