package com.hudz;

import com.hudz.Forest.Oak;
import com.hudz.Zoo.Lion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application
{
    private static final Logger logger = LogManager.getLogger(Application.class.getName());

    public static void main(String[] args)
    {
        new Oak().namePlate();
        new Lion().voice();
        logger.trace("This is a trace message");
        logger.debug("This is a debug message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.error("Error Message Logged !!!", new NullPointerException("NullError"));
    }
}
